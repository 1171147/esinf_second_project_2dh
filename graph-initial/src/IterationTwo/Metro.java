/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;

/**
 *
 * @author pedro
 */
public class Metro {

    Graph<Estacao, String> metro;

    public Metro(Graph<Estacao, String> graph) {
        this.metro = graph;
    }

    public Metro(HashMap<String, String> mapa, ArrayList<String> linhas) {
        this.metro = new Graph<>(true);
        for (String linha : linhas) {
            String[] linhaAux = linha.split(";");
            String coordenadas = mapa.get(linhaAux[1]);
            String[] coord = coordenadas.split(";");
            Estacao e = new Estacao(linhaAux[1], Double.parseDouble(coord[0]), Double.parseDouble(coord[1]), linhaAux[0]);
            Estacao e1 = new Estacao(linhaAux[2], Double.parseDouble(coord[0]), Double.parseDouble(coord[1]), linhaAux[0]);
            inserirEstacao(e);
            inserirEstacao(e1);
            inserirLinha(e, e1, linhaAux[0], Double.parseDouble(linhaAux[3]));
        }
    }

    public void inserirEstacao(Estacao e) {
        Estacao e1 = new Estacao(e.getNomeEstacao(), e.getLatitude(), e.getLongitude(), "Central");
        this.metro.insertVertex(e1);
        this.metro.insertVertex(e);
        inserirLinha(e, e1, "Central", 0);
        inserirLinha(e1, e, "Central", 0);

    }

    public boolean inserirLinha(Estacao orig, Estacao dest, String eInf, double weight) {
        return this.metro.insertEdge(orig, dest, eInf, weight);
    }

    public ArrayList<LinkedList<Estacao>> verificarConectividade() {
        Estacao[] conjuntoEstacoes = metro.allkeyVerts();
        LinkedList<Estacao> conjunto = GraphAlgorithms.BreadthFirstSearch(metro, conjuntoEstacoes[0]);
        ArrayList<LinkedList<Estacao>> componentes = new ArrayList<>();

        componentes.add(conjunto);
        if (conjuntoEstacoes.length != conjunto.size()) {
            for (int i = 1; i < conjuntoEstacoes.length; i++) {
                if (!verificaEstacaoExisteEmConjuntos(conjuntoEstacoes[i], componentes)) {
                    conjunto = new LinkedList<>();
                    conjunto = GraphAlgorithms.BreadthFirstSearch(metro, conjuntoEstacoes[i]);
                    componentes.add(conjunto);
                }
            }
            return componentes;
        }
        return null;

    }

    public boolean verificaEstacaoExisteEmConjuntos(Estacao e, ArrayList<LinkedList<Estacao>> conjuntos) {
        for (LinkedList<Estacao> conjunto : conjuntos) {
            if (conjunto.contains(e)) {
                return true;
            }
        }
        return false;
    }

    public Estacao getEstacaoByName(String nomeEstacao) {
        for (Estacao e : metro.allkeyVerts()) {
            if (e.getNomeEstacao().equals(nomeEstacao)) {
                return e;
            }
        }
        return null;
    }

    public Percurso shortestPathUnweightedStation(String Orig, String Dest, LinkedList<Estacao> shortPath) {
        Estacao Origem = getEstacaoByName(Orig);
        Estacao Destino = getEstacaoByName(Dest);
        Graph<Estacao, String> metroClone = this.metro.clone();
        for (Edge e : metroClone.edges()) {
            if (e.getElement() != "Central") {
                e.setWeight(1);
            }
        }
        double estacoes = GraphAlgorithms.shortestPath(metroClone, Origem, Destino, shortPath);

        return new Percurso(new Metro(metroClone), shortPath, 0.0, estacoes);
    }

    public Percurso shortestPathWeightedStation(String Orig, String Dest, LinkedList<Estacao> shortPath, double tempoOriginal) {
        Estacao Origem = getEstacaoByName(Orig);
        Estacao Destino = getEstacaoByName(Dest);
        double distancia = GraphAlgorithms.shortestPath(this.metro, Origem, Destino, shortPath);
        distancia += tempoOriginal;
        Percurso p = new Percurso(this, shortPath, tempoOriginal, distancia);
        return p;
    }

    public Percurso shortestPathWeightedIntermediate(String Orig, String Dest, LinkedList<Estacao> intermedias, LinkedList<Estacao> shortPath, double tempoOriginal) {
        if (intermedias.isEmpty()) {
            return null;
        }

        Estacao origem = getEstacaoByName(Orig);
        Estacao destino = getEstacaoByName(Dest);
        LinkedList<Estacao> percursoOrdenado = new LinkedList<>();
        percursoOrdenado.add(origem);
        double custo = ordenarEstacoesIntermedias(percursoOrdenado, origem, intermedias, destino);

        return new Percurso(this, percursoOrdenado, tempoOriginal, custo);
    }

    public Double ordenarEstacoesIntermedias(LinkedList<Estacao> percursoOrdenado, Estacao Origem, LinkedList<Estacao> intermedias, Estacao Destino) {

        double custo = 0;
        LinkedList<Estacao> shortPath = new LinkedList<>();
        LinkedList<Estacao> interShortPath = new LinkedList<>();
        while (!intermedias.isEmpty()) {
            double minimo = Double.MAX_VALUE;
            Estacao interDest = new Estacao();

            for (Estacao e : intermedias) {
                double distancia = GraphAlgorithms.shortestPath(this.metro, Origem, e, shortPath);
                if (distancia < minimo) {
                    minimo = distancia;
                    interDest = e;
                    interShortPath.clear();
                    while (!shortPath.isEmpty()) {
                        interShortPath.addLast(shortPath.remove());
                    }
                }
            }

            interShortPath.removeFirst();
            percursoOrdenado.addAll(interShortPath);
            custo += minimo;
            Origem = interDest;
            intermedias.remove(interDest);

        }

        double distancia = GraphAlgorithms.shortestPath(this.metro, percursoOrdenado.getLast(), Destino, shortPath);
        shortPath.removeFirst();
        percursoOrdenado.addAll(shortPath);
        custo += distancia;
        return custo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.metro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Metro other = (Metro) obj;
        return this.metro.equals(other.metro);
    }

    public double getWeightEdge(Estacao Orig, Estacao Dest) {
        return this.metro.getEdge(Orig, Dest).getWeight();
    }

    public Percurso shortestPathLines(String Orig, String Dest) {
        LinkedList<Estacao> burn = new LinkedList<>();
        Estacao Origem = getEstacaoByName(Orig);
        Estacao Destino = getEstacaoByName(Dest);
        Graph<Estacao, String> metroClone = this.metro.clone();
        for (Edge e : metroClone.edges()) {
            if (e.getElement() == "Central") {
                e.setWeight(666);
            }
        }
        double estacoes = GraphAlgorithms.shortestPath(metroClone, Origem, Destino, burn);

        return new Percurso(this, burn, 0.0, 0);
    }

}
