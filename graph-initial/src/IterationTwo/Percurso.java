/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author pedro
 */
public class Percurso {

    private Map<String, Set<Estacao>> a;
    private LinkedList<Estacao> shortPath;
    private double[] tempos;
    private double tInicial;
    private double custoTotal;

    Percurso(Metro m, LinkedList<Estacao> shortPath, double tInicial, double custoTotal) {
        this.shortPath = shortPath;
        this.tInicial = tInicial;
        this.custoTotal = custoTotal;
        this.a = new HashMap<>();
        fillEstacoes();
        this.tempos = new double[this.shortPath.size()];
        this.tempos[0] = tInicial;
        for (int i = 1; i < this.shortPath.size(); i++) {
            this.tempos[i] = m.getWeightEdge(this.shortPath.get(i - 1), this.shortPath.get(i)) + this.tempos[i - 1];
        }
    }

    private void fillEstacoes() {
        for (Estacao e : this.shortPath) {
            String numLinha = e.getCodigoLinha();
            if (this.a.get(numLinha) == null) {
                this.a.put(numLinha, new HashSet<Estacao>());
            }
            this.a.get(numLinha).add(e);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.shortPath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Percurso other = (Percurso) obj;
        return this.shortPath.equals(other.shortPath);
    }

    @Override
    public String toString() {
        String s = "Custo Total do Percurso: " + custoTotal + "\n";
        int i = 0;
        for (Estacao estacao : shortPath) {
            s += estacao.toString() + "\t\t";
            s += "I : " + tempos[i];
            s += "\n";
            i++;
        }

        return s;
    }

    public Map<String, Set<Estacao>> getMap() {
        return a;
    }

    public void setCustoTotal(double custoTotal) {
        this.custoTotal = custoTotal;
    }

}
