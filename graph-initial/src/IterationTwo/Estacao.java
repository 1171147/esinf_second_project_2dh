/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import java.util.Objects;

/**
 *
 * @author Dekas
 */
public class Estacao {
    
    private String nomeEstacao;
    private double latitude; 
    private double longitude;
    private String codigoLinha;
    
    public Estacao() {
    nomeEstacao = null;
    latitude = 0;
    longitude = 0;
    }
    
    public Estacao(String nomeEstacao, double latitude, double longitude) {
        this.nomeEstacao = nomeEstacao;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Estacao(String nomeEstacao, double latitude, double longitude, String codigoLinha) {
        this.nomeEstacao = nomeEstacao;
        this.latitude = latitude;
        this.longitude = longitude;
        this.codigoLinha = codigoLinha;
    }
    
    
    public Estacao(Estacao e) {
        e.nomeEstacao = this.nomeEstacao;
        e.latitude = this.latitude;
        e.longitude = this.longitude;
    }

    public Estacao(String nomeEstacao, String codigoLinha) {
        this.nomeEstacao = nomeEstacao;
        this.codigoLinha = codigoLinha;
    }
    

    public String getNomeEstacao() {
        return nomeEstacao;
    }
    
    public String getCodigoLinha() {
        return codigoLinha;
    }

    public void setNomeEstacao(String nomeEstacao) {
        this.nomeEstacao = nomeEstacao;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return this.nomeEstacao + "-" + this.codigoLinha;
                }


@Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.nomeEstacao);
        hash = 17 * hash + Objects.hashCode(this.codigoLinha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null||getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        return this.nomeEstacao.equals(other.nomeEstacao) &&
                this.codigoLinha.equals(other.codigoLinha);
    }
    
}
