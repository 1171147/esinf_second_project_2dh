/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Dekas
 */
public class Import {


    /*
    * devolve a lista de estacoes
     */
    public static ArrayList<String> lerConections(String nomeFicheiro) {
        ArrayList<String> linhas = new ArrayList<>();
        try {
            File csv = new File(nomeFicheiro);
            Scanner sc = new Scanner(csv);

            while (sc.hasNext()) {
                String line = sc.nextLine();
                linhas.add(line);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("File not Found: " + ex);
        }
        return linhas;
    }

    /*
    * Devolve um mapa
     */
    public static Metro lerCordenadas(String nomeFicheiro,String nomeFicheiro2) {
        ArrayList<String> linhas = lerConections(nomeFicheiro2);
        HashMap<String, String> estacoes = new HashMap<>();
        try {
            File csv = new File(nomeFicheiro);
            Scanner sc = new Scanner(csv);

            while (sc.hasNext()) {
                String line = sc.nextLine();
                String s[] = line.split(";");
                estacoes.put(s[0], s[1] + ";" + s[2]);

            }

        } catch (FileNotFoundException ex) {
            System.out.println("File not Found: " + ex);
        }
        return new Metro(estacoes, linhas);
    }

}
