/*
* A collection of graph algorithms.
 */
package graphbase;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vert
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        qbfs.add(vert);
        LinkedList<V> qaux = new LinkedList<>();
        qaux.add(vert);

        while (!qaux.isEmpty()) {
            V vertex = qaux.removeFirst();
            for (V v : g.adjVertices(vertex)) {
                if (!qbfs.contains(v)) {
                    qbfs.add(v);
                    qaux.add(v);

                }
            }
        }

        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {
        visited[g.getKey(vOrig)] = true;
        qdfs.add(vOrig);
        for (V adjVertice : g.adjVertices(vOrig)) {
            if (!visited[g.getKey(adjVertice)]) {
                DepthFirstSearch(g, adjVertice, visited, qdfs);
            }
        }
    }

    /**
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vert
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {
        LinkedList<V> qdfs = new LinkedList<>();
        boolean visited[] = new boolean[g.numVertices()];
        if (!g.validVertex(vert)) {
            return null;
        }
        DepthFirstSearch(g, vert, visited, qdfs);
        return qdfs;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited, LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        visited[g.getKey(vOrig)] = true;
        path.add(vOrig);
        for (V vAdj : g.adjVertices(vOrig)) {
            if (vAdj.equals(vDest)) {
                path.add(vDest);
                paths.add(revPath(path));
                path.removeLast();
            } else {
                if (!visited[g.getKey(vAdj)]) {
                    allPaths(g, vAdj, vDest, visited, path, paths);
                }
            }

        }
        V lastV = path.removeLast();
        visited[g.getKey(lastV)] = false;
    }

    /**
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vOrig
     * @param vDest
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return paths;
        }
        boolean visited[] = new boolean[g.numVertices()];
        LinkedList<V> path = new LinkedList<>();
        allPaths(g, vOrig, vDest, visited, path, paths);
        return paths;

    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vertices
     * @param visited set of discovered vertices
     * @param pathKeys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        dist[g.getKey(vOrig)] = 0;
        int index = g.getKey(vOrig);
        while (index != -1) {
            vOrig = vertices[index];
            visited[g.getKey(vOrig)] = true;
            for (V vAdj : g.adjVertices(vOrig)) {
                Edge<V, E> edge = g.getEdge(vOrig, vAdj);
                double edgeWeight = edge.getWeight() + dist[g.getKey(vOrig)];
                if (!visited[g.getKey(vAdj)] && dist[g.getKey(vAdj)] > edgeWeight) {
                    dist[g.getKey(vAdj)] = dist[g.getKey(vOrig)] + edge.getWeight();
                    pathKeys[g.getKey(vAdj)] = g.getKey(vOrig);
                }

            }
            index = getVertMinDist(dist, visited);

        }
    }

    private static int getVertMinDist(double[] dist, boolean[] visited) {
        double min = Double.MAX_VALUE;
        int minVertIndex = -1;
        for (int i = 0; i < dist.length; i++) {
            if (!visited[i] && dist[i] < min) {
                min = dist[i];
                minVertIndex = i;
            }
        }
        return minVertIndex;
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vOrig
     * @param vDest
     * @param verts
     * @param pathKeys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {
        path.addFirst(vDest);
        if (!vDest.equals(vOrig)) {
            int vDestKey = g.getKey(vDest);
            int prev = pathKeys[vDestKey];
            vDest = verts[prev];
            getPath(g, vOrig, vDest, verts, pathKeys, path);
        }
    }

    /*
    * TODO : shortest-path between vOrig and vDest
     */
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {
        shortPath.clear();
        if (!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return 0;
        }
        if (vOrig.equals(vDest)) {
            shortPath.add(vDest);
            return 1;
        }
        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts];
        int[] pathkeys = new int[nverts];
        double[] dist = new double[nverts];
        for (int i = 0; i < nverts; i++) {
            pathkeys[i] = -1;
            dist[i] = Double.MAX_VALUE;
            visited[i] = false;
        }
        shortestPathLength(g, vOrig, g.allkeyVerts(), visited, pathkeys, dist);
        if (pathkeys[g.getKey(vDest)] == -1) {
            return 0;
        }
        getPath(g, vOrig, vDest, g.allkeyVerts(), pathkeys, shortPath);
        return dist[g.getKey(vDest)];
    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {
        // validamos se o vertice é valido
        if (!g.validVertex(vOrig)) {
            return false;
        }
        // numero de vertices do grafo
        int nverts = g.numVertices();
        //array de vertices visitados
        boolean[] visited = new boolean[nverts]; //default value: false
        //array de chaves do caminho
        int[] pathKeys = new int[nverts];
        //array de distancias
        double[] dist = new double[nverts];
        // array de todos os vertices do grafo
        V[] vertices = g.allkeyVerts();
        /*
        * loop para definir todos as distancias ao valor máximo
         */
        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        //caminho mais curto entre o vertice origem e todos os outros vertices
        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);
        // limpa as distancias
        dists.clear();
        // limpa os caminhos
        paths.clear();
        /*
        * todos os caminhos e distancias ficam null
         */
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        /*
        * 
         */
        for (int i = 0; i < nverts; i++) {
            LinkedList<V> shortPath = new LinkedList<>();
            if (dist[i] != Double.MAX_VALUE) {
                getPath(g, vOrig, vertices[i], vertices, pathKeys, shortPath);
            }
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }



}
