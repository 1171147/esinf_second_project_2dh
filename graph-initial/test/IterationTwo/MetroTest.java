/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import graphbase.Graph;
import java.util.ArrayList;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Dekas
 */
public class MetroTest {

    Graph<Estacao, String> instance = new Graph<>(true);

    public MetroTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of inserirEstacao method, of class Metro.
     */
    @Test
    public void testInserirEstacao() {

    }

    /**
     * Test of inserirConexao method, of class Metro.
     */
    @Test
    public void testInserirConexao() {

    }

    /**
     * Test of verificarConectividade method, of class Metro.
     */
    @Test
    public void testVerificarConectividade() {

        System.out.println("### Verificar Conectividade: Grafo Conexo ###");
        Metro instance = Import.lerCordenadas("coordinates.csv", "connections.csv");
        ArrayList<LinkedList<Estacao>> expResult = null;
        ArrayList<LinkedList<Estacao>> result = instance.verificarConectividade();
        assertEquals(expResult, result);

        System.out.println("### Verificar Conectividade: Grafo Não-Conexo ###");
        instance = Import.lerCordenadas("testCoord.csv", "testGraph.csv");
        int expResult2 = 2;
        ArrayList<LinkedList<Estacao>> result2 = instance.verificarConectividade();
        assertEquals(expResult2, result2.size());

        Estacao test = new Estacao("Test", 5, 6, "5");
        Estacao test2 = new Estacao("Test2", 5, 6, "5");
        instance.inserirEstacao(test2);
        instance.inserirLinha(test, test2, "5", 4);
        expResult2 = 4;
        result2 = instance.verificarConectividade();
        assertEquals(expResult2, result2.size());
    }

    /**
     * Test of getMetro method, of class Metro.
     */
    @Test
    public void testGetMetro() {

    }

    /**
     * Test of shortestPathUnweighted method, of class Metro.
     */
    @Test
    public void testShortestPathWeightedStation() {
        System.out.println("### Test Shortest Path Weighted ###");
        Metro m = Import.lerCordenadas("testCoordShortestPath.csv", "testConecShortestPath.csv");
        String orig = "La Defense";
        String dest = "Pont de Levallois Becon";
        LinkedList<Estacao> shortPathResult = new LinkedList<>();
        Percurso result = m.shortestPathWeightedStation(orig, dest, shortPathResult, 0);

        LinkedList<Estacao> shortPathExpected = new LinkedList<>();

        Estacao LaDefenseCentral = new Estacao("La Defense", 2.33855, 48.8843, "Central");
        Estacao LaDefense2 = new Estacao("La Defense", 2.33855, 48.8843, "2");
        Estacao PortDauphine2 = new Estacao("Porte Dauphine", 2.39581, 48.85466, "2");
        Estacao PortDauphineCentral = new Estacao("Porte Dauphine", 2.39581, 48.85466, "Central");
        Estacao PortDauphine3 = new Estacao("Porte Dauphine", 2.39581, 48.85466, "3");
        Estacao vitorHugo3 = new Estacao("Victor Hugo", 2.30155, 48.86462, "3");
        Estacao anatoleFrance3 = new Estacao("Anatole France", 2.28491, 48.89224, "3");
        Estacao anatoleFranceCentral = new Estacao("Anatole France", 2.28491, 48.89224, "Central");
        Estacao anatoleFrance4 = new Estacao("Anatole France", 2.28491, 48.89224, "4");
        Estacao pontBeacon4 = new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "4");
        Estacao pontBeaconCentral = new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "Central");
        shortPathExpected.add(LaDefenseCentral);
        shortPathExpected.add(LaDefense2);
        shortPathExpected.add(PortDauphine2);
        shortPathExpected.add(PortDauphineCentral);
        shortPathExpected.add(PortDauphine3);
        shortPathExpected.add(vitorHugo3);
        shortPathExpected.add(anatoleFrance3);
        shortPathExpected.add(anatoleFranceCentral);
        shortPathExpected.add(anatoleFrance4);
        shortPathExpected.add(pontBeacon4);
        shortPathExpected.add(pontBeaconCentral);

        Percurso expected = new Percurso(m, shortPathExpected, 0, 4);

        assertEquals(expected, result);
    }

    /**
     * Test of shortestPathUnweighted method, of class Metro.
     */
    @Test
    public void testShortestPathWeightedStationStores() {
        System.out.println("### Test Shortest Path Weighted Professor###");
        Metro m = Import.lerCordenadas("coordinates.csv", "connections.csv");
        String orig = "Nation";
        String dest = "Telegraphe";
        LinkedList<Estacao> shortPathResult = new LinkedList<>();
        Percurso result = m.shortestPathWeightedStation(orig, dest, shortPathResult, 0);
        System.out.println("### Nation - Telegraphe ###");
        System.out.println(result.toString());

        String orig1 = "Buzenval";
        String dest1 = "Franklin Roosevelt";
        LinkedList<Estacao> shortPathResult1 = new LinkedList<>();
        Percurso result1 = m.shortestPathWeightedStation(orig1, dest1, shortPathResult1, 0);
        System.out.println("### Buzenval - Fraklin Roosevelt ###");
        System.out.println(result1.toString());

        String orig2 = "Nation";
        String dest2 = "Termes";
        LinkedList<Estacao> shortPathResult2 = new LinkedList<>();
        Percurso result2 = m.shortestPathWeightedStation(orig2, dest2, shortPathResult2, 0);
        System.out.println("### Nation - Termes ###");
        System.out.println(result2.toString());
    }

    /**
     * Test of shortestPathUnweighted method, of class Metro.
     */
    @Test
    public void testShortestPathUnweightedStation() {
        System.out.println("Test of shortest path unweighted");
        LinkedList<Estacao> shortPath = new LinkedList<Estacao>();
        Metro instance = Import.lerCordenadas("testCoordShortestPath.csv", "testConecShortestPath.csv");

        LinkedList<Estacao> shortPathEsperado = new LinkedList<>();

        Estacao laDefense = new Estacao("La Defense", 2.33855, 48.8843, "Central");
        Estacao pontBecon = new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "Central");

        shortPathEsperado.add(laDefense);
        shortPathEsperado.add(new Estacao("La Defense", 2.33855, 48.8843, "1"));
        shortPathEsperado.add(new Estacao("Concorde", 2.32111, 48.86549, "1"));
        shortPathEsperado.add(new Estacao("Concorde", 2.32111, 48.86549, "Central"));
        shortPathEsperado.add(new Estacao("Concorde", 2.32111, 48.86549, "6"));
        shortPathEsperado.add(new Estacao("Argentine", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(pontBecon);

        Percurso esperado = new Percurso(instance, shortPathEsperado, 0.0, 3.0);
        Percurso result = instance.shortestPathUnweightedStation("La Defense", "Pont de Levallois Becon", shortPath);

        assertEquals(esperado, result);
    }

    @Test
    public void testShortestPathUnweightedStationFicheiroOriginal() {
        System.out.println();
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Test of shortest path unweighted original file");
        System.out.println("Nation -> Telegraphe");
        Metro instance = Import.lerCordenadas("coordinates.csv", "connections.csv");
        LinkedList<Estacao> shortPath = new LinkedList<Estacao>();
        Percurso result = instance.shortestPathUnweightedStation("Nation", "Telegraphe", shortPath);
        System.out.println(result.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");

        System.out.println("Test of shortest path unweighted original file");
        System.out.println("Buzenval -> Franklin Roosevelt");
        Percurso result1 = instance.shortestPathUnweightedStation("Buzenval", "Franklin Roosevelt", shortPath);
        System.out.println(result1.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");

        System.out.println("Test of shortest path unweighted original file");
        System.out.println("Nation -> Termes");
        Percurso result2 = instance.shortestPathUnweightedStation("Nation", "Termes", shortPath);
        System.out.println(result2.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");
    }

    @Test
    public void testShortestPathWeightedIntermediateFicheiroOriginal() {
        System.out.println("TESTE CAMINHO MAIS CURTO COM ESTAÇÕES INTERMEDIAS COM FICHEIRO ORIGINAL");
        Metro instance = Import.lerCordenadas("coordinates.csv", "connections.csv");
        LinkedList<Estacao> shortPath = new LinkedList<Estacao>();
        LinkedList<Estacao> intermedias = new LinkedList<Estacao>();
        intermedias.add(instance.getEstacaoByName("Republique"));
        intermedias.add(instance.getEstacaoByName("Bastille"));
        Percurso result = instance.shortestPathWeightedIntermediate("Nation", "Telegraphe", intermedias, shortPath, 2.0);
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Nation -> Telegraphe passando por Republique e Bastille");
        System.out.println(result.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");

        intermedias = new LinkedList<Estacao>();
        intermedias.add(instance.getEstacaoByName("Iena"));
        intermedias.add(instance.getEstacaoByName("Bercy"));
        intermedias.add(instance.getEstacaoByName("Pasteur"));
        Percurso result1 = instance.shortestPathWeightedIntermediate("Buzenval", "Franklin Roosevelt", intermedias, shortPath, 2.0);
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Buzenval -> Franklin Roosevelt passando por Iena, Bercy e Pasteur");
        System.out.println(result1.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");

        intermedias = new LinkedList<Estacao>();
        intermedias.add(instance.getEstacaoByName("Dupleix"));
        intermedias.add(instance.getEstacaoByName("Bercy"));
        intermedias.add(instance.getEstacaoByName("Madeleine"));
        Percurso result2 = instance.shortestPathWeightedIntermediate("Convention", "Trocadero", intermedias, shortPath, 2.0);
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Convention -> Trocadero passando por Dupleix, Bercy e Madeleine");
        System.out.println(result2.toString());
        System.out.println("///////////////////////////////////////////////////////////////////////////////////");

    }

    @Test
    public void testShortestPathWeightedIntermediate() {
        System.out.println("Test of shortest path with intermediate station");
        LinkedList<Estacao> shortPath = new LinkedList<Estacao>();
        Metro instance1 = Import.lerCordenadas("testCoordShortestPath.csv", "testConecShortestPath.csv");

        LinkedList<Estacao> shortPathEsperado = new LinkedList<>();

        Estacao laDefense = new Estacao("La Defense", 2.33855, 48.8843, "Central");
        Estacao pontBecon = new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "Central");

        shortPathEsperado.add(laDefense);
        shortPathEsperado.add(new Estacao("La Defense", 2.33855, 48.8843, "2"));
        shortPathEsperado.add(new Estacao("Porte Dauphine", 2.39581, 48.85466, "2"));
        shortPathEsperado.add(new Estacao("Porte Dauphine", 2.39581, 48.85466, "Central"));
        shortPathEsperado.add(new Estacao("Porte Dauphine", 2.39581, 48.85466, "3"));
        shortPathEsperado.add(new Estacao("Victor Hugo", 2.30155, 48.86462, "3"));
        shortPathEsperado.add(new Estacao("Victor Hugo", 2.30155, 48.86462, "Central"));
        shortPathEsperado.add(new Estacao("Victor Hugo", 2.30155, 48.86462, "3"));
        shortPathEsperado.add(new Estacao("Anatole France", 2.28491, 48.89224, "3"));
        shortPathEsperado.add(new Estacao("Anatole France", 2.28491, 48.89224, "Central"));
        shortPathEsperado.add(new Estacao("Anatole France", 2.28491, 48.89224, "4"));
        shortPathEsperado.add(new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "4"));
        shortPathEsperado.add(pontBecon);
        shortPathEsperado.add(new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(new Estacao("Argentine", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(new Estacao("Argentine", 2.29011, 48.87537, "Central"));
        shortPathEsperado.add(new Estacao("Argentine", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(new Estacao("Pont de Levallois Becon", 2.29011, 48.87537, "6"));
        shortPathEsperado.add(pontBecon);

        Percurso esperado = new Percurso(instance1, shortPathEsperado, 0.0, 10.0);
        LinkedList<Estacao> intermedias1 = new LinkedList<>();
        intermedias1.add(instance1.getEstacaoByName("Victor Hugo"));
        intermedias1.add(instance1.getEstacaoByName("Argentine"));
        Percurso result = instance1.shortestPathWeightedIntermediate("La Defense", "Pont de Levallois Becon", intermedias1, shortPath, 0);

        assertEquals(esperado, result);

    }

    @Test
    public void testCaminhoMinimoNrmLinhasTestesOriginal() {
        System.out.println("////////////////////////////////////////////////");
        System.out.println("## Caminho Minimo Por Linhas - Ficheiro Original ##");
        System.out.println("////////////////////////////////////////////////");
        Metro instance = Import.lerCordenadas("coordinates.csv", "connections.csv");
        System.out.println("### Nation - Telegraphe ###");
        Percurso result = instance.shortestPathLines("Nation", "Telegraphe");
        int resultInt = result.getMap().size() - 2;
        result.setCustoTotal(resultInt);
        System.out.println(result.toString());

        System.out.println("### Buzenval - Franklin Roosevelt ###");
        result = instance.shortestPathLines("Buzenval", "Franklin Roosevelt");
        resultInt = result.getMap().size() - 2;
        result.setCustoTotal(resultInt);
        System.out.println(result.toString());

        System.out.println("### Convention - Trocadero ###");
        result = instance.shortestPathLines("Convention", "Trocadero");
        resultInt = result.getMap().size() - 2;
        result.setCustoTotal(resultInt);
        System.out.println(result.toString());

    }

    @Test
    public void testCaminhoMinimoNrmLinhasTestesTester() {
        System.out.println("////////////////////////////////////////////////");
        System.out.println("## Caminho Minimo Por Linhas - Ficheiro Teste ##");
        System.out.println("////////////////////////////////////////////////");
        Metro instance = Import.lerCordenadas("testCoordShortestPath.csv", "testConecShortestPath.csv");
        Percurso result = instance.shortestPathLines("La Defense", "Pont de Levallois Becon");
        System.out.println(result.toString());
        int resultInt = result.getMap().size() - 2;
        int expResult = 0;
        assertEquals(expResult, resultInt);

    }
}


