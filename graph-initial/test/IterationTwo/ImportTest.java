/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IterationTwo;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Dekas
 */
public class ImportTest {
    
    public ImportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerEstacoes method, of class Import.
     */
    @Test
    public void testLerEstacoes() {
        
        Metro m = Import.lerCordenadas("testCoord.csv", "testGraph.csv");
        HashMap<String,String> mapa = new HashMap<>();
        ArrayList<String> linhas = new ArrayList<>();
        String ele = "1;La Defense;Esplanade de La Defense;1";
        String ele1 = "1;Esplanade de La Defense;La Defense;1";
        String ele2 = "2;Porte Dauphine;Victor Hugo;1";
        String ele3 = "2;Victor Hugo;Porte Dauphine;1";
        String ele4 = "3;Pont de Levallois Becon;Anatole France;1";
        String ele5 = "3;Anatole France;Pont de Levallois Becon;1";
        String ele6 = "5;Victor Hugo;Pont de Levallois Becon;1";
        linhas.add(ele);
        linhas.add(ele1);
        linhas.add(ele2);
        linhas.add(ele3);
        linhas.add(ele4);
        linhas.add(ele5);
        linhas.add(ele6);
        
        String mapa1 = "La Defense";
        String mapa1s = "2.33855;48.8843";
        String mapa2 = "Esplanade de La Defense";
        String mapa2s = "2.32652;48.82825";
        String mapa3 = "Porte Dauphine";
        String mapa3s = "2.39581;48.85466";
        String mapa4 = "Victor Hugo";
        String mapa4s = "2.30155;48.86462";
        String mapa5 = "Anatole France";
        String mapa5s = "2.28491;48.89224";
        String mapa6 = "Pont de Levallois Becon";
        String mapa6s = "2.29011;48.87537";
        mapa.put(mapa1, mapa1s);
        mapa.put(mapa2, mapa2s);
        mapa.put(mapa3, mapa3s);
        mapa.put(mapa4, mapa4s);
        mapa.put(mapa5, mapa5s);
        mapa.put(mapa6, mapa6s);
        Metro m1 = new Metro(mapa, linhas);
        
        assertEquals(m,m1);
        
    }

    /**
     * Test of lerLinhas method, of class Import.
     */
    @Test
    public void testLerLinhas() {

    }
    
}
